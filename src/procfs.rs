use std::{
    ffi,
    fmt,
    fs,
    io::{
        self,
        ErrorKind,
    },
    path::{
        Path,
        PathBuf,
    }
};
use log::{
    debug,
};

trait StrStripExt {
    fn strip_suffix_optional<'a, S>(&'a self, suffix: S, recursive: bool) -> &'a Self where
        S: AsRef<str>;
}

impl StrStripExt for str {
    // TODO: Make sure this actually gets tailrec optimized
    fn strip_suffix_optional<'a, S>(&'a self, suffix: S, recursive: bool) -> &'a Self where
        S: AsRef<str>,
    {
        match (self.strip_suffix(suffix.as_ref()), recursive) {
            (Some(s), true) => s.strip_suffix_optional(suffix, true),
            (Some(s), false) => s,
            (None, _) => self,
        }
    }
}

fn to_io_error<S, T>(prefix: Option<S>, e: &T) -> io::Error where
    S: fmt::Display,
    T: fmt::Debug,
{
    let err_with_string = |s: String| io::Error::new(ErrorKind::Other, s);
    prefix
        .map(|prefix| err_with_string(format!("{}: {:?}", prefix, e)))
        .unwrap_or_else(|| err_with_string(format!("{:?}", e)))
}

pub fn pidof_multiple<It>(params: It) -> io::Result<Option<libc::pid_t>> where
    It: IntoIterator,
    It::Item: AsRef<ffi::OsStr>,
{
    use std::process::{
        Command,
        Stdio,
    };
    let params = params.into_iter();
    let mut cmd = Command::new("pidof");
    let output = params.fold(&mut cmd, |cmd, arg| cmd.arg(arg))
        .stdin(Stdio::null())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .output()?;
    std::str::from_utf8(output.stdout.as_slice())
        .map(|s| s.strip_suffix_optional("\n", true))
        .map_err(|e| to_io_error(Some("Invalid utf-8 output from \"pidof\""), &e))
        .and_then(|s| {
            let s = match s.split(" ").next() {
                Some(s) if s.len() > 0 => s,
                _ => {
                    return Ok(None);
                },
            };
            s.parse::<libc::pid_t>()
                .map(Some)
                .map_err(|e| to_io_error(Some("Invalid numer output from \"pidof\""), &e))
        })
}

#[inline(always)]
pub fn pidof<S: AsRef<ffi::OsStr>>(search: S) -> io::Result<Option<libc::pid_t>> {
    use std::iter;
    pidof_multiple(iter::once(search))
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct ProcfsProcess {
    pid: libc::pid_t,
}

impl ProcfsProcess {
    #[inline(always)]
    pub fn new(pid: libc::pid_t) -> Self {
        ProcfsProcess {
            pid: pid,
        }
    }

    #[inline]
    pub fn directory(&self) -> PathBuf {
        let mut p = PathBuf::from("/proc");
        p.push(format!("{}", self.pid));
        p
    }

    #[inline]
    pub fn subpath<P: AsRef<Path>>(&self, subpath: P) -> PathBuf {
        let mut p = self.directory();
        p.push(subpath);
        p
    }

    pub fn cmdline(&self) -> io::Result<Vec<String>> {
        use io::{
            BufRead,
            BufReader,
        };
        let p = self.subpath("cmdline");
        let f = fs::OpenOptions::new()
            .read(true)
            .open(p)
            .map(BufReader::new)?;
        let cmdline = match f.lines().next().and_then(|v| v.ok()) {
            Some(v) => v,
            None => {
                return Ok(Vec::new());
            },
        };
        debug!("raw_cmdline = {:?}", &cmdline);

        let ret = cmdline
            .strip_suffix_optional("\0", false)
            .split("\0")
            .map(String::from)
            .collect();
        Ok(ret)
    }
}
