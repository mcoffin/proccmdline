use clap::{
    App,
    crate_name,
    crate_description,
    crate_authors,
    crate_version,
};

#[inline(always)]
pub fn crate_app() -> App<'static, 'static> {
    App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .author(crate_authors!())
}
