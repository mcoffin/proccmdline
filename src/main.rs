extern crate clap;
extern crate libc;
extern crate log;
extern crate env_logger;

mod clap_ext;
mod procfs;

use std::{
    fmt,
    io::{
        self,
        ErrorKind,
    },
};
use clap::ArgMatches;
use log::{
    debug,
};

mod args {
    pub const PID: &'static str = "pid";
    pub const PROCESS: &'static str = "process";
    pub const DEBUG_FORMAT: &'static str = "debug-format";

    pub const PIDSPEC_GROUP: &'static str = "pidspec";
}

fn app() -> clap::App<'static, 'static> {
    use clap::{
        Arg,
        ArgGroup,
    };
    let pid_arg = Arg::with_name(args::PID)
        .long("pid")
        .short("p")
        .takes_value(true)
        .help("PID from which to get command line");
    let process_arg = Arg::with_name(args::PROCESS)
        .help("Search input for \"pidof\" used to find PID");
    let debug_format_arg = Arg::with_name(args::DEBUG_FORMAT)
        .long("debug-format")
        .short("d")
        .takes_value(false)
        .help("Use debug formatter for formatting arguments");
    let pidspec_group = ArgGroup::with_name(args::PIDSPEC_GROUP)
        .args(&[args::PID, args::PROCESS])
        .required(true);
    clap_ext::crate_app()
        .arg(pid_arg)
        .arg(process_arg)
        .arg(debug_format_arg)
        .group(pidspec_group)
}

#[derive(Debug, Clone, Copy)]
enum PidSpec<'a> {
    Pid(libc::pid_t),
    Process(&'a str),
}

impl<'a> PidSpec<'a> {
    fn get_pid(&self) -> io::Result<libc::pid_t> {
        match *self {
            PidSpec::Pid(pid) => Ok(pid),
            PidSpec::Process(s) => {
                procfs::pidof(s)
                    .and_then(|pid| {
                        pid.map(Ok).unwrap_or_else(|| Err(io::Error::new(ErrorKind::Other, format!("Process not found: {}", s))))
                    })
            },
        }
    }
}

trait ArgMatchesExt {
    fn pidspec<'a>(&'a self) -> PidSpec<'a>;
}

impl<'matches> ArgMatchesExt for ArgMatches<'matches> {
    fn pidspec<'a>(&'a self) -> PidSpec<'a> {
        self.value_of(args::PID)
            .map(|s| s.parse::<libc::pid_t>().expect("Invalid PID"))
            .map(PidSpec::Pid)
            .unwrap_or_else(|| PidSpec::Process(self.value_of(args::PROCESS).unwrap()))
    }
}

fn print_arg_normal<S: fmt::Display>(arg: S) {
    println!("{}", arg);
}

fn print_arg_debug<S: fmt::Debug>(arg: S) {
    println!("{:?}", arg);
}

fn main() {
    use procfs::ProcfsProcess;
    env_logger::init();
    let matches = app().get_matches();
    let pid = matches.pidspec().get_pid()
        .expect("Failed to search for PID");
    debug!("pid = {}", pid);
    let cmdline = ProcfsProcess::new(pid).cmdline()
        .expect("Failed to get cmdline");
    let print_arg = if matches.is_present(args::DEBUG_FORMAT) {
        print_arg_debug
    } else {
        print_arg_normal
    };
    for arg in cmdline {
        print_arg(arg);
    }
}
